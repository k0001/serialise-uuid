# serialise-uuid

Encode and decode UUID values in CBOR
using [uuid-types](
https://hackage.haskell.org/package/uuid-types
), [cborg](
https://hackage.haskell.org/package/cborg
) and [serialise](
https://hackage.haskell.org/package/serialise
).

# Development

* Build all with `nix-build`.

* Build with some GHC or GHCJS version with `nix-build -A $xxx`, where `$xxx` is
  one of `ghc865`, `ghc883`, `ghcjs86`.

* Enter a development environment with `nix-shell -A $xxx.env`, where `$xxx` is
  one of `ghc865`, `ghc883`, `ghcjs86`.
