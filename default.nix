{ nixpkgs ? fetchTarball
    { url = "https://github.com/NixOS/nixpkgs-channels/archive/1e90c46c2d98f9391df79954a74d14f263cad729.tar.gz";
      sha256 = "1xs0lgh3q1hbrj0lbpy3czw41cv6vxx9kdf2npwc58z8xq3sdqmh"; },
  pkgs ? import nixpkgs {}
}:
{
  ghc865 = pkgs.haskell.packages.ghc865.callPackage ./pkg.nix {};
  ghc883 = pkgs.haskell.packages.ghc883.callPackage ./pkg.nix {};
  ghcjs86 = pkgs.haskell.packages.ghcjs86.callPackage ./pkg.nix {};
}
