{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE ScopedTypeVariables #-}

module Main
  ( main,
  )
where

import qualified Codec.Serialise as Ser
import Codec.Serialise.UUID ()
import qualified Data.ByteString.Lazy as BL
import qualified Data.UUID.Types as UUID
import qualified Test.Tasty as Tasty
import Test.Tasty.HUnit ((@?=), testCase)
import Test.Tasty.QuickCheck ((===), Gen, arbitrary, forAll, testProperty)
import qualified Test.Tasty.Runners as Tasty

main :: IO ()
main =
  Tasty.defaultMainWithIngredients
    [ Tasty.consoleTestReporter,
      Tasty.listingTests
    ]
    tt

tt :: Tasty.TestTree
tt =
  Tasty.testGroup
    "serialise-uuid"
    [ testProperty "round trip" $ forAll genUUID $ \u ->
        Right u === Ser.deserialiseOrFail (Ser.serialise u),
      testProperty "expected format" $ forAll genUUID $ \u ->
        Ser.serialise u === "\xd8\x25\x50" <> UUID.toByteString u,
      testProperty "expected length" $ forAll genUUID $ \u ->
        BL.length (Ser.serialise u) === 19,
      testCase "00000000-0000-0000-0000-000000000000" $
        Ser.serialise UUID.nil @?= "\xd8\x25\x50\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00",
      testCase "8b0d1a20-dcc5-11d9-bda9-0002a5d5c51b" $
        let Just u = UUID.fromString "8b0d1a20-dcc5-11d9-bda9-0002a5d5c51b"
         in Ser.serialise u @?= "\xd8\x25\x50\x8b\x0d\x1a\x20\xdc\xc5\x11\xd9\xbd\xa9\x00\x02\xa5\xd5\xc5\x1b"
    ]

genUUID :: Gen UUID.UUID
genUUID =
  UUID.fromWords
    <$> arbitrary
    <*> arbitrary
    <*> arbitrary
    <*> arbitrary
