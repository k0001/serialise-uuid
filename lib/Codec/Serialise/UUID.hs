-- | This module offers an orphan 'Ser.Serialise' instance for 'UUID.UUID'.
--
-- Import as follows:
--
-- @
-- import Codec.Serialise.UUID ()
-- @
module Codec.Serialise.UUID () where

import qualified Codec.Serialise as Ser
import qualified Data.UUID.Types as UUID

import qualified Codec.CBOR.UUID

-- | Encoded according to [draft-bormann-cbor-tags-oid-06](https://tools.ietf.org/html/draft-bormann-cbor-tags-oid-06).
--
-- 19 bytes consisting of @0xd8@, @0x25@, @0x50@, and the 16 raw bytes of the
-- UUID in network order. For example, the UUID @8b0d1a20-dcc5-11d9-bda9-0002a5d5c51b@ is
-- encoded as @"\\xd8\\x25\\x50\\x8b\\x0d\\x1a\\x20\\xdc\\xc5\\x11\\xd9\\xbd\\xa9\\x00\\x02\\xa5\\xd5\\xc5\\x1b"@.
instance Ser.Serialise UUID.UUID where
  encode = Codec.CBOR.UUID.encode
  {-# INLINE encode #-}
  decode = Codec.CBOR.UUID.decode
  {-# INLINE decode #-}
