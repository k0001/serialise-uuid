{ mkDerivation, base, serialise, stdenv, tasty, tasty-hunit
, tasty-quickcheck, uuid-types
}:
mkDerivation {
  pname = "serialise-uuid";
  version = "0.1";
  src = ./.;
  libraryHaskellDepends = [ base serialise uuid-types ];
  testHaskellDepends = [
    base serialise tasty tasty-hunit tasty-quickcheck
  ];
  homepage = "https://gitlab.com/k0001/serialise-uuid";
  description = "Encode and decode UUID values in CBOR using uuid-types, cborg and serialise";
  license = stdenv.lib.licenses.bsd3;
}
